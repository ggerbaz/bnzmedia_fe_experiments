function bnzm_header() {

  var h = this;

  h.init = function() {

    h.el          = $('#nhbzm');
    h.strip       = $('#strip_adv');
    h.doc         = $('body');
    h.menu        = $('#nnbzm');
    h.menuHandle  = $('#nhmh');
    h.container   = $('#container');
    h.vpos        = $(window).scrollTop();

    h.create();

  }

  h.create = function() {

    h.menuCloseHandle = $(document.createElement('A')).addClass('nh_menu_close_handle').text('chiudi').appendTo( h.menu );

    h.react();
  }

  h.react = function() {

    // SCROLL

    // header visibile / strip nascosta al caricamento di pagina: refresh / history back
    h.justLoaded = true;

    $(window).scroll( 
      function() { 
        if (!h.justLoaded) {
          h.showhide();
        }
        h.justLoaded = false;
      } 
    );    

    // MENU'

    h.menuHandle.on(
      'click',
      function(e) {

        e.preventDefault();
        h.el.css({'width':document.documentElement.clientWidth});
        h.menu.css({'width':document.documentElement.clientWidth});
        h.doc.addClass('nnbzm_open');

      }
    );

    h.menuCloseHandle.on(
      'click',
      function(e) {

        e.preventDefault();
        h.doc.removeClass('nnbzm_open');
        setTimeout(
          function() {
            h.el.css({'width':'100%'});
            h.menu.css({'width':'100%'});
          },
          525
        );
        
      }
    );    

  }

  h.showhide = function() {    

    vpos = $(window).scrollTop();

    if (vpos > h.vpos) {

      h.el.removeClass('visible');
      h.strip.addClass('visible');

    }
    else {

      h.el.addClass('visible');
      h.strip.removeClass('visible');

    }

    h.vpos = vpos;

  }

}







if (document.addEventListener) {
  document.addEventListener("mousewheel", MouseWheelHandler(), false);
  document.addEventListener("DOMMouseScroll", MouseWheelHandler(), false);
} else {
  sq.attachEvent("onmousewheel", MouseWheelHandler());
}

function MouseWheelHandler() {
    return function (e) {
        if ( $('body').hasClass('nmbzm_open') || $('body').hasClass('tendina_noscroll') ) {
          return false;  
        } else {
          return
        }
    }
}

var prevents = false;
$(window).on('touchstart', function(e) {
  if ( $('body').hasClass('nmbzm_open') || $('body').hasClass('tendina_noscroll') ) {
  
    if (document.documentElement.clientWidth>670 ) {

      if ($(e.target, '#nmbzm').length == 1 ) prevents = false;
      else prevents = true

      $("#nhOverlayer").on("touchmove", false);

    }

  } else {
      $("#bnzm_container").on("touchmove", true);    
  }
});

$(window).on('touchend', function(e) {
  prevents = false;
});

$(window).on('touchmove', function(e) {
  if (prevents) e.preventDefault();
});


