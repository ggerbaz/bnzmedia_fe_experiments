function bnzm_sidebar() {

  var s = this;

  s.compute = function() {

    s.sw = document.documentElement.clientWidth;

    s.eltop = s.elcont.offset();
    s.eltop = s.eltop.top;
    s.elheight = (s.sw>1539) ? s.el.height()+s.wideScreenAdvBorder : s.el.height();
  
    s.boxtop = s.box.offset();
    s.boxtop = s.boxtop.top;
    s.boxheight = s.box.height();

    s.pagetop = s.page.offset();
    s.pagetop = s.pagetop.top;    

    s.expheight = s.exp.height();
    
    s.topLimit = s.boxtop;
    s.botLimit = s.boxtop+s.boxheight-s.eltop+s.sidebarBoxMarginBottom;

    s.vpos = $(window).scrollTop();    
    
    s.isRedrawing = false;

  }

  s.init = function(){

      s.el = $('#gpt-container-1');
      s.elcont = $('.right');
      s.box = $('#jump');
      s.page = $('#container');
      s.exp = $('#strip_adv');  
      s.isDown = false;
      s.isRedrawing = false;

      s.wideScreenAdvBorder = 60;
      s.sidebarBoxMarginBottom = 40;

      setTimeout(
        function(){
          s.compute();
          $(window).scroll(function() { if (!s.isRedrawing) s.refresh(); });
        },
        //1500
        750
      );      

      $(window).on('resize', function() { if (!s.isRedrawing) s.reDraw(); });

  }

  s.refresh = function() {
    
    var sw = document.documentElement.clientWidth,
    vpos = $(window).scrollTop(),
    //adjust = (vpos>s.vpos) ? 25 : 75;
    adjust = 75;

    if (sw>950) {

      if (s.expheight != $('#strip_adv').height()) {
        s.compute();
      }

      s.elpos = s.el.offset();
      s.elpos = s.elpos.top;

      if (!s.isDown) {
        
        if ((vpos-s.eltop) >= (adjust*-1)) {

          s.el.css({'position':'fixed','top':adjust});

          if ((vpos+s.elheight+adjust+s.sidebarBoxMarginBottom)>=s.topLimit) {
             s.el.css({'position':'','top':s.topLimit-s.eltop-s.elheight-s.sidebarBoxMarginBottom});
          }
        } else {
          s.el.css({'position':'','top':0});
        }

        if (s.elpos+s.elheight<vpos) {
          s.el.css({'position':'','top':s.botLimit});
          s.isDown = true;
        }

      } else {
     
        if((vpos-s.eltop)-s.botLimit >= (adjust*-1)) {
          s.el.css({'position':'fixed','top':adjust});
        } else {
          s.el.css({'position':'','top':s.botLimit});
          if ( s.botLimit>=(vpos+70+50+15+s.expheight+15+s.elheight+s.elheight) ) {
            s.el.css({'position':'','top':s.topLimit-s.eltop-s.elheight-s.sidebarBoxMarginBottom});
            s.isDown = false;
          }
        }
      }
      
      s.vpos = vpos;

    }

  };

  s.reDraw = function() {

    var sw = document.documentElement.clientWidth;

    if ( (sw >= 950 && s.sw < 950) ||  (sw >= 1540 && s.sw < 1540) ) {

      setTimeout(
        function(){
          s.isRedrawing = true;
          s.compute();
        },
        350
      );   

      s.sw = sw;  

    }     

  };

}

function bnzm_swapadv() {

  var swap = this;

  swap.init = function() {

    swap.desktop = $('#gpt-container-1');
    swap.mobile = $('#gpt-container-mobile-1');
    swap.sw = document.documentElement.clientWidth;
    $(window).resize(function() { swap.refresh(); });

  };

  swap.refresh = function() {

    var sw = document.documentElement.clientWidth;

    // sidebar 
    if (sw >= 930 && swap.sw < 930) {

      swap.desktop.html(swap.mobile.html());
      swap.mobile.html('');
      if (typeof googletag === "undefined") {} else { 
        googletag.pubads().refresh([gslots[0]]); 
      }
      

    // no sidebar
    } else if (sw < 930 && swap.sw >= 930) {

      swap.mobile.html(swap.desktop.html());
      swap.desktop.html('');
      if (typeof googletag === "undefined") {} else { 
        googletag.pubads().refresh([gslots[0]]); 
      }

    }

    swap.sw = sw;

  };

}